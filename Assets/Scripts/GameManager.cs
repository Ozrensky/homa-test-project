﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homa;

public class GameManager : MonoBehaviour
{
    #region private properties
    private int achievedLevel = 1;
    private int gems = 0;
    private int score = 0;

    bool detectorFlag = false;
    bool incrementFlag = false;
    #endregion

    private void Awake()
    {
        //quality settings
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        GlobalManager.GameManager = this;
        //getting save data
        achievedLevel = PlayerPrefs.GetInt("achievedLevel", 1);
        gems = PlayerPrefs.GetInt("gems", 0);
        score = PlayerPrefs.GetInt("score", 0);
    }

    // Start is called before the first frame update
    void Start()
    {
        GlobalManager.LevelManager.GenerateLevel();
        GlobalManager.UIManager.ShowMainMenu();
    }

    //begin level 
    public void StartLevel()
    {
        detectorFlag = true;
        incrementFlag = true;
        GlobalManager.UIManager.UpdateProgressLevels();
        GlobalManager.UIManager.ShowPlayPanel();
        GlobalManager.Player.Play();
    }

    //finished level
    public void LevelFinished()
    {
        if (incrementFlag)
        {
            incrementFlag = false;
            GlobalManager.LevelManager.GetActiveLevel().SetCounting(false);
            GlobalManager.UIManager.ToggleTrackProgress(false);
            IncrementLevel();
            GlobalManager.AudioManager.PlayWin();
            GlobalManager.Player.Finish();
            GlobalManager.UIManager.ShowWinPanel();
            Debug.Log("Level finished");
        }
    }

    //start new level
    public void RestartLevel()
    {
        incrementFlag = true;
        detectorFlag = true;
        GlobalManager.LevelManager.GenerateLevel();
        GlobalManager.UIManager.ShowPlayPanel();
    }

    //level failed
    public void Lose()
    {
        if (detectorFlag)
        {
            GlobalManager.UIManager.ToggleTrackProgress(false);
            GlobalManager.LevelManager.GetActiveLevel().SetCounting(false);
            detectorFlag = false;
            incrementFlag = false;
            GlobalManager.AudioManager.PlayLose();
            Debug.Log("Level failed");
            GlobalManager.Player.Lose();
            GlobalManager.PoolManager.SpawnParticle("DestroyPlayer", GlobalManager.Player.transform);
        }
    }

    public int GetAchievedLevel()
    {
        return achievedLevel;
    }

    public int GetGems()
    {
        return gems;
    }

    public int GetScore()
    {
        return score;
    }

    public void AddScore(int value)
    {
        GlobalManager.AudioManager.PlayScore();
        score += value;
        PlayerPrefs.SetInt("score", score);
        GlobalManager.UIManager.UpdateScore(score, true);
    }

    public void ResetScore()
    {
        PlayerPrefs.SetInt("score", score = 0);
        GlobalManager.UIManager.UpdateScore(score);
    }

    public void AddGem()
    {
        gems++;
        PlayerPrefs.SetInt("gems", gems);
        GlobalManager.UIManager.UpdateGems(gems, true);
        GlobalManager.AudioManager.PlayGem();
    }

    //increase level number 
    public void IncrementLevel()
    {
        achievedLevel++;
        PlayerPrefs.SetInt("achievedLevel", achievedLevel);
    }
}
