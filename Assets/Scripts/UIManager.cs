﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Homa;
using TMPro;

public class UIManager : MonoBehaviour
{
    #region inspector properties
    [SerializeField]
    private GameObject mainMenuPanel;
    [SerializeField]
    private GameObject playPanel;
    [SerializeField]
    private GameObject winPanel;
    [SerializeField]
    private Image soundImage;
    [SerializeField]
    private TextMeshProUGUI currentLevel;
    [SerializeField]
    private Slider progressBar;
    [SerializeField]
    private TextMeshProUGUI gems;
    [SerializeField]
    private TextMeshProUGUI score;
    [SerializeField]
    private TextMeshProUGUI scoreAdd;
    [SerializeField]
    private Image fadeImage;
    [SerializeField]
    private float fadeInDuration = 0.5f;
    [SerializeField]
    private float fadeOutDuration = 0.5f;
    #endregion

    #region private properties
    private Coroutine fadeCoroutine;
    private float progress = 0f;
    private bool trackProgress = false;
    #endregion

    private void Awake()
    {
        GlobalManager.UIManager = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //progress slider update
        if (trackProgress)
        {
            progress += Time.deltaTime;
            float value = 1 / (GlobalManager.LevelManager.GetActiveLevel().GetDistance() / progress);
            progressBar.value = value;
        }
    }

    //toggle slider progress tracking
    public void ToggleTrackProgress(bool value)
    {
        progress = 0f;
        trackProgress = value;
    }

    //update sound image
    public void UpdateSoundImage(bool value)
    {
        if (value)
        {
            soundImage.color = Color.black;
        }
        else
        {
            soundImage.color = Color.grey;
        }
    }

    //fade in
    public void FadeIn()
    {
        if (fadeCoroutine != null)
        {
            StopCoroutine(fadeCoroutine);
        }
        if (fadeImage)
        {
            fadeCoroutine = StartCoroutine(FadeInCoroutine());
        }
    }

    //fade in coroutine
    IEnumerator FadeInCoroutine()
    {
        fadeImage.gameObject.SetActive(true);
        float elapsed = 0;
        while (elapsed <= fadeInDuration)
        {
            elapsed += Time.deltaTime;
            Color c = fadeImage.color;
            c.a = Mathf.Lerp(1f, 0f, elapsed/ fadeInDuration);
            fadeImage.color = c;

            yield return new WaitForEndOfFrame();
        }
        fadeImage.gameObject.SetActive(false);
        GlobalManager.Player.Play();
    }

    //fade out
    public void FadeOut()
    {
        if (fadeCoroutine != null)
        {
            StopCoroutine(fadeCoroutine);
        }
        if (fadeImage)
        {
            fadeCoroutine = StartCoroutine(FadeOutCoroutine());
        }
    }

    //fade out coroutine
    IEnumerator FadeOutCoroutine()
    {
        winPanel.SetActive(false);
        fadeImage.gameObject.SetActive(true);
        float elapsed = 0;
        while (elapsed <= fadeOutDuration)
        {
            elapsed += Time.deltaTime;
            Color c = fadeImage.color;
            c.a = Mathf.Lerp(0f, 1f, elapsed / fadeOutDuration);
            fadeImage.color = c;

            yield return new WaitForEndOfFrame();
        }
        GlobalManager.GameManager.RestartLevel();

        yield return new WaitForSeconds(1f);
        FadeIn();
    }

    public void ShowMainMenu()
    {
        mainMenuPanel.SetActive(true);
        if (winPanel.activeSelf)
            winPanel.SetActive(false);
        if (playPanel.activeSelf)
            playPanel.SetActive(false);
    }

    public void ShowWinPanel()
    {
        winPanel.SetActive(true);
        if (mainMenuPanel.activeSelf)
            mainMenuPanel.SetActive(false);
        if (playPanel.activeSelf)
            playPanel.SetActive(false);
    }

    public void ShowPlayPanel()
    {
        playPanel.SetActive(true);
        if (winPanel.activeSelf)
            winPanel.SetActive(false);
        if (mainMenuPanel.activeSelf)
            mainMenuPanel.SetActive(false);
        if (winPanel.activeSelf)
            winPanel.SetActive(false);
    }

    //setting current level text
    public void UpdateProgressLevels()
    {
        currentLevel.text = "LEVEL " + GlobalManager.GameManager.GetAchievedLevel().ToString();
    }

    public void UpdateGems(int value, bool isAdd = false)
    {
        if (isAdd)
        {
            if (gems.GetComponent<Animator>())
            {
                gems.GetComponent<Animator>().SetTrigger("pulse");
            }
        }
        gems.text = value.ToString();
    }

    public void UpdateScore(int value, bool isAdd = false, int addedValue = 0)
    {
        if (isAdd)
        {
            scoreAdd.text = "+" + (value - int.Parse(score.text));
            if (score.GetComponent<Animator>())
            {
                score.GetComponent<Animator>().SetTrigger("pulse");
            }
            if (scoreAdd.GetComponent<Animator>())
            {
                scoreAdd.GetComponent<Animator>().SetTrigger("scoreAdd");
            }
        }
        score.text = value.ToString();
    }

    //reset progress slider
    public void ResetProgressBar()
    {
        progressBar.value = 0;
    }
}

