﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Homa
{
    public static class GlobalManager
    {
        private static GameManager gameManager;
        public static GameManager GameManager
        {
            get
            {
                if (gameManager != null)
                {
                    gameManager = Object.FindObjectOfType<GameManager>();
                    if (gameManager == null)
                    {
                        Debug.Log("GameManager doesn't exist in Scene!");
                    }
                }
                return gameManager;
            }
            set { gameManager = value; }
        }

        private static UIManager uiManager;
        public static UIManager UIManager
        {
            get
            {
                if (uiManager != null)
                {
                    uiManager = Object.FindObjectOfType<UIManager>();
                    if (uiManager == null)
                    {
                        Debug.Log("UIManager doesn't exist in Scene!");
                    }
                }
                return uiManager;
            }
            set { uiManager = value; }
        }

        private static AudioManager audioManager;
        public static AudioManager AudioManager
        {
            get
            {
                if (audioManager != null)
                {
                    audioManager = Object.FindObjectOfType<AudioManager>();
                    if (audioManager == null)
                    {
                        Debug.Log("AudioManager doesn't exist in Scene!");
                    }
                }
                return audioManager;
            }
            set { audioManager = value; }
        }

        private static PoolManager poolManager;
        public static PoolManager PoolManager
        {
            get
            {
                if (poolManager != null)
                {
                    poolManager = Object.FindObjectOfType<PoolManager>();
                    if (poolManager == null)
                    {
                        Debug.Log("PoolManager doesn't exist in Scene!");
                    }
                }
                return poolManager;
            }
            set { poolManager = value; }
        }

        private static LevelManager levelManager;
        public static LevelManager LevelManager
        {
            get
            {
                if (levelManager != null)
                {
                    levelManager = Object.FindObjectOfType<LevelManager>();
                    if (levelManager == null)
                    {
                        Debug.Log("LevelManager doesn't exist in Scene!");
                    }
                }
                return levelManager;
            }
            set { levelManager = value; }
        }

        private static PlayerController player;
        public static PlayerController Player
        {
            get
            {
                if (player != null)
                {
                    player = Object.FindObjectOfType<PlayerController>();
                    if (player == null)
                    {
                        Debug.Log("Player doesn't exist in Scene!");
                    }
                }
                return player;
            }
            set { player = value; }
        }
    }
}
