﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homa;

public class LevelManager : MonoBehaviour
{
    #region inspector properties
    [SerializeField]
    private Transform levelHolder;
    [SerializeField]
    private List<Level> levels;
    [SerializeField]
    private List<Color> cameraBg;
    #endregion

    private Level activeLevel;
    private List<Level> usedLevels;

    private void Awake()
    {
        levels = new List<Level>();
        usedLevels = new List<Level>();
        PopulateLevels();
        GlobalManager.LevelManager = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {

    }

    //return random color from color list
    private Color GetRandomColor()
    {
        if (cameraBg.Count == 0)
        {
            return Color.blue;
        }
        else
        {
            int rn = Random.Range(0, cameraBg.Count);
            return cameraBg[rn];
        }
    }

    //spawn level
    public void GenerateLevel()
    {
        Camera.main.backgroundColor = GetRandomColor();
        if (activeLevel != null)
        {
            activeLevel.gameObject.SetActive(false);
        }
        int num = Random.Range(0, levels.Count);
        activeLevel = levels[num];
        usedLevels.Add(activeLevel);
        levels.Remove(activeLevel);
        if (levels.Count == 0)
        {
            levels.AddRange(usedLevels);
            usedLevels.Clear();
        }
        activeLevel.gameObject.SetActive(true);
        GlobalManager.Player.SpawnPlayer(activeLevel.GetStartPos());
    }

    //get levels from level holder
    public void PopulateLevels()
    {
        if (levelHolder)
        {
            foreach (Transform child in levelHolder)
            {
                Level level = child.gameObject.GetComponent<Level>();
                levels.Add(level);
            }
        }
        else
        {
            Debug.Log("Level Holder doesnt exist");
        }
    }

    public int GetLevelCount()
    {
        return levels.Count;
    }

    public Level GetActiveLevel()
    {
        return activeLevel;
    }
}
