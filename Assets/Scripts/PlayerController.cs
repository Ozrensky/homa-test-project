﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homa;

public class PlayerController : MonoBehaviour
{
    #region inspector properties
    [SerializeField]
    private float normalVelocity = 5;
    [SerializeField]
    private float speedUpVelocity = 10;
    [SerializeField]
    private float slopeForce;
    [SerializeField]
    private float slopeForceRayLength;
    [SerializeField]
    private float loseForce;
    [Header("Trails")]
    [SerializeField]
    private GameObject[] regularTrails;
    [Header("Camera")]
    [SerializeField]
    Transform idleCameraPos;
    [SerializeField]
    Transform playCameraPos;
    [SerializeField]
    Transform winCameraPos;
    [SerializeField]
    float winCamTransDuration = 0.5f;
    [SerializeField]
    float playCamTransDuration = 0.5f;
    [SerializeField]
    private Animator normalCharAnimator;
    [SerializeField]
    private SkinnedMeshRenderer normalCharMesh;
    [SerializeField]
    private SkinnedMeshRenderer ragdollMesh;
    [SerializeField]
    private Rigidbody ragdollCenter;
    #endregion

    #region private properties
    private Rigidbody rb;
    private Animator animator;
    private Collider[] allColliders;
    private Rigidbody[] allRb;
    private float legSpread;
    private Transform mainCam;
    private Vector3 cameraStartPos;
    private Quaternion cameraStartRot;
    private Vector3 cameraEndPos;
    private Quaternion cameraEndRot;
    private Coroutine cameraCoroutine;
    private bool inAir = false;
    private bool speedUp = false;
    private bool isPlaying = false;
    #endregion

    private void Awake()
    {
        GlobalManager.Player = this;
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        mainCam = Camera.main.transform;
        allColliders = GetComponentsInChildren<Collider>(true);
        allRb = GetComponentsInChildren<Rigidbody>(true);
        ToggleRagdoll(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlaying)
        {
            //leg spread for mobiles
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                legSpread += Input.touches[0].deltaPosition.x * 0.01f;
                UpdateSpread(legSpread);
            }

            //leg spread for pc
            #if UNITY_EDITOR
            legSpread += Input.GetAxis("Horizontal") * 0.05f;
            UpdateSpread(legSpread);
            #endif

            //normal movement velocity add
            if (rb.velocity.z < normalVelocity)
            {
                AddNormalVelocity();
            }

            //rotating player so it is always aligned to surface
            if (!inAir)
            {
                RaycastHit hit;
                Ray ray = new Ray(transform.position, -transform.up);

                if (Physics.Raycast(ray, out hit, slopeForceRayLength))
                {
                    rb.rotation  = Quaternion.FromToRotation(transform.up, hit.normal) *
                    transform.rotation;

                    transform.rotation = Quaternion.Lerp(transform.rotation, rb.rotation,
                    Time.deltaTime * 5);
                }
            }

            //speed up movement velocity add
            if (speedUp)
            {
                if (rb.velocity.z < speedUpVelocity)
                {
                    AddSpeedUpVelocity();
                }
            }

            //adding down force so that player doesnt jump when going down slope
            if (OnSlope())
            {
                rb.AddForce(Vector3.down * slopeForce * Time.deltaTime);
                if (rb.velocity.z < speedUpVelocity)
                {
                    AddSpeedUpVelocity();
                }
            } 
        }
    }

    //checking if the player is on slope
    private bool OnSlope()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, slopeForceRayLength))
        {
            if (hit.normal != Vector3.up)
                return true;
        }
        return false;
    }

    //ragdoll toggle function
    public void ToggleRagdoll(bool enabled)
    {
        foreach (Collider c in allColliders)
        {
            if (c.GetComponent<ObstacleDetector>() == null && c.GetComponent<PlayerController>() == null)
            {
                c.enabled = enabled;
            }
        }
        foreach (Rigidbody rb in allRb)
        {
            if (rb.GetComponent<ObstacleDetector>() == null)
            {
                rb.useGravity = enabled;
                rb.isKinematic = !enabled;
            }
        }
        rb.useGravity = !enabled;
        rb.isKinematic = enabled;
        animator.enabled = !enabled;
        GetComponent<CapsuleCollider>().enabled = !enabled;
        if (enabled)
            GetComponent<BoxCollider>().enabled = false;
    }

    //start play function
    public void Play()
    {
        GlobalManager.LevelManager.GetActiveLevel().SetCounting(true);
        GlobalManager.UIManager.ToggleTrackProgress(true);
        ToggleRagdoll(false);
        AddNormalVelocity();
        SetPlayCam();
        ClearTrails();
        TurnOnTrails();
        isPlaying = true;
    }

    //lose
    public void Lose()
    {
        ToggleRagdoll(true);
        ragdollCenter.AddForce(transform.forward * loseForce) ;
        isPlaying = false;
        mainCam.parent = null;
        rb.isKinematic = true;
        StartCoroutine(WaitForFadeOut());
    }

    //after lose wait coroutine
    IEnumerator WaitForFadeOut() {
        yield return new WaitForSeconds(2f);

        GlobalManager.UIManager.FadeOut();
    }

    //finish
    public void Finish()
    {
        isPlaying = false;
        SetWinCam();
        GlobalManager.PoolManager.SpawnParticle("WinGame 1", mainCam);
        normalCharAnimator.ResetTrigger("reset");
        normalCharMesh.enabled = true;
        ragdollMesh.enabled = false;
        normalCharAnimator.SetTrigger("dance");
    }

    //adding normal movement velocity
    public void AddNormalVelocity()
    {
        rb.velocity = transform.forward * normalVelocity;
    }

    //adding speed up velocity
    public void AddSpeedUpVelocity()
    {
        rb.velocity = transform.forward * speedUpVelocity;
        //rb.AddForce(transform.forward * speedUpVelocity * 100);
    }

    //spawn player, reset
    public void SpawnPlayer(Transform spawn)
    {
        GlobalManager.UIManager.UpdateProgressLevels();
        GlobalManager.UIManager.ResetProgressBar();
        GlobalManager.PoolManager.GetParticle("WinGame 1").gameObject.SetActive(false);
        animator.ResetTrigger("reset");
        inAir = false;
        speedUp = false;
        normalCharMesh.enabled = false;
        ragdollMesh.enabled = true;
        UpdateSpread(0);
        ToggleRagdoll(false);
        SetIdleCam();
        TurnOffTrails();
        rb.isKinematic = false;
        animator.SetTrigger("reset");
        normalCharAnimator.SetTrigger("reset");
        legSpread = 0f;
        transform.position = spawn.position;
    }

    //updating spread float in animator
    private void UpdateSpread(float value)
    {
        if (value > 1)
        {
            legSpread = 1;
        }
        else if (value < 0)
        {
            legSpread = 0;
        }
        else
        {
            legSpread = value;
        }
        animator.SetFloat("spread", legSpread);
        normalCharAnimator.SetFloat("spread", legSpread);
    }

    //switching to idle cam
    public void SetIdleCam()
    {
        mainCam.SetParent(idleCameraPos);
        mainCam.localPosition = Vector3.zero;
        mainCam.localRotation = Quaternion.identity;
    }

    //switching to play cam
    private void SetPlayCam()
    {
        if (cameraCoroutine != null)
        {
            StopCoroutine(cameraCoroutine);
        }
        mainCam.SetParent(playCameraPos);
        cameraStartPos = mainCam.localPosition;
        cameraStartRot = mainCam.localRotation;
        cameraEndPos = Vector3.zero;
        cameraEndRot = Quaternion.identity;
        cameraCoroutine = StartCoroutine(CameraTranslate(playCamTransDuration));
    }

    //switching to win cam
    private void SetWinCam()
    {
        if (cameraCoroutine != null)
        {
            StopCoroutine(cameraCoroutine);
        }
        mainCam.SetParent(winCameraPos);
        cameraStartPos = mainCam.localPosition;
        cameraStartRot = mainCam.localRotation;
        cameraEndPos = Vector3.zero;
        cameraEndRot = Quaternion.identity;
        cameraCoroutine = StartCoroutine(CameraTranslate(winCamTransDuration));
    }

    //camera switch coroutine
    IEnumerator CameraTranslate(float duration)
    {
        float elapsed = 0;
        while (elapsed <= duration)
        {
            elapsed += Time.deltaTime;
            mainCam.localPosition = Vector3.Lerp(cameraStartPos, cameraEndPos, elapsed / duration);
            mainCam.localRotation = Quaternion.Slerp(cameraStartRot, cameraEndRot, elapsed / duration);

            yield return new WaitForEndOfFrame();
        }
    }

    public void SetInAir(bool value)
    {
        GetComponent<BoxCollider>().enabled = value;
        inAir = value;
    }

    public void TurnOffTrails()
    {
        if (regularTrails != null)
        {
            foreach (GameObject t in regularTrails)
            {
                t.SetActive(false);
            }
        }
    }

    public void TurnOnTrails()
    {
        if (regularTrails != null)
        {
            foreach (GameObject t in regularTrails)
            {
                t.SetActive(true);
            }
        }
    }

    //trails reset
    public void ClearTrails()
    {
        foreach (GameObject t in regularTrails)
        {
            if (t.GetComponent<TrailRenderer>())
                t.GetComponent<TrailRenderer>().Clear();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "FinishLine")
        {
            GlobalManager.GameManager.LevelFinished();
        }

        if (other.tag == "Air")
        {
            SetInAir(true);
        }

        if (other.tag == "SpeedUp")
        {
            speedUp = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Air")
        {
            SetInAir(false);
        }

        if (other.tag == "SpeedUp")
        {
            speedUp = false;
        }
    }
}
