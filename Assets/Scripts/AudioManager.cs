﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homa;

public class AudioManager : MonoBehaviour
{
    #region inspector properties
    [Header("Sources")]
    [SerializeField]
    private AudioSource playerSource;
    [SerializeField]
    private AudioSource environmentSource;
    [SerializeField]
    private AudioSource backgroundSource;
    [SerializeField]
    private AudioSource uiSource;
    [SerializeField]
    private AudioSource scoreSource;
    [Header("Clips")]
    [SerializeField]
    private AudioClip win;
    [SerializeField]
    private AudioClip lose;
    [SerializeField]
    private AudioClip gem;
    [SerializeField]
    private AudioClip score;
    [SerializeField]
    private AudioClip tap;
    #endregion

    private int muted = 0;

    private void Awake()
    {
        GlobalManager.AudioManager = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //loading mute data
        muted = PlayerPrefs.GetInt("muted", 0);
        UpdateSources();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //updating sources, mute
    public void UpdateSources()
    {
        if (muted == 0)
        {
            if (playerSource)
                playerSource.mute = false;
            if (environmentSource)
                environmentSource.mute = false;
            if (backgroundSource)
                backgroundSource.mute = false;
            if (uiSource)
                uiSource.mute = false;
            GlobalManager.UIManager.UpdateSoundImage(true);
        }
        else
        {
            if (playerSource)
                playerSource.mute = true;
            if (environmentSource)
                environmentSource.mute = true;
            if (backgroundSource)
                backgroundSource.mute = true;
            if (uiSource)
                uiSource.mute = true;
            GlobalManager.UIManager.UpdateSoundImage(false);
        }
    }

    public void PlayWin()
    {
        if (muted == 0 && playerSource != null & win != null)
        {
            playerSource.clip = win;
            playerSource.Play();
        }
    }

    public void PlayLose()
    {
        if (muted == 0 && playerSource != null & lose != null)
        {
            playerSource.clip = lose;
            playerSource.Play();
        }
    }

    public void PlayGem()
    {
        if (muted == 0 && playerSource != null & gem != null)
        {
            playerSource.clip = gem;
            playerSource.Play();
        }
    }

    public void PlayScore()
    {
        if (muted == 0 && scoreSource != null & score != null)
        {
            scoreSource.clip = score;
            scoreSource.Play();
        }
    }

    public void PlayTap()
    {
        if (muted == 0 && uiSource != null & tap != null)
        {
            uiSource.clip = tap;
            uiSource.Play();
        }
    }

    //mute toggle
    public void ToggleMute()
    {
        if (muted == 0)
        {
            muted = 1;
            UpdateSources();
        }
        else
        {
            muted = 0;
            UpdateSources();
        }
        PlayerPrefs.SetInt("muted", muted);
    }
}
