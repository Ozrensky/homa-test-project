﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homa;

public class ObstacleDetector : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            GlobalManager.GameManager.Lose();
        }
    }
}
