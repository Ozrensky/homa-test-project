﻿using System.Collections;
using System.Collections.Generic;
using Homa;
using UnityEngine;

public class TurnTrigger : MonoBehaviour
{
    #region inspector properties
    [SerializeField] Transform nextPoint;
    [SerializeField] float turnDuration;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //if player enters a turn trigger, rotate him towards next point
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(Turn());
        }
    }

    IEnumerator Turn()
    {
        float elapsed = 0;
        Quaternion playerRot = GlobalManager.Player.transform.rotation;
        while (elapsed <= turnDuration)
        {
            elapsed += Time.deltaTime;
            GlobalManager.Player.transform.rotation = Quaternion.Slerp(playerRot, nextPoint.rotation, elapsed/turnDuration);

            yield return new WaitForEndOfFrame();
        }
    }
}
