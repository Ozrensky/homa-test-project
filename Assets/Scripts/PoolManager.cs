﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homa;

public class PoolManager : MonoBehaviour
{
    #region inspector properties
    [SerializeField]
    private List<ParticleSystem> particles;
    [SerializeField]
    private Transform particlesHolder;
    #endregion

    private void Awake()
    {
        GlobalManager.PoolManager = this;
        PopulateParticles();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //get particles from particle holder
    public void PopulateParticles()
    {
        if (particlesHolder)
        {
            foreach (Transform child in particlesHolder)
            {
                particles.Add(child.GetComponent<ParticleSystem>());
            }
        }
    }

    //spawn particle by name
    public void SpawnParticle(string name, Transform parent = null)
    {
        foreach (ParticleSystem p in particles)
        {
            if (p.gameObject.name == name)
            {
                if (parent != null)
                    p.gameObject.transform.SetParent(parent, false);
                p.gameObject.SetActive(true);
                p.Play();
            }
        }
    }

    //return particle by name
    public ParticleSystem GetParticle(string name)
    {
        ParticleSystem particle = new ParticleSystem();
        foreach (ParticleSystem p in particles)
        {
            if (p.gameObject.name == name)
            {
                particle = p;
            }
        }

        if (particle != null)
            return particle;
        else
        {
            Debug.Log("Particle doesn't exist.");
            return null;
        }
    }
}
