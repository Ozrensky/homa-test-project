﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    #region inspector properties
    public float distanceCount = 0f;
    [SerializeField]
    private Transform startPos;
    [SerializeField]
    private float distance;
    #endregion

    private bool counting = false;

    //return player start position
    public Transform GetStartPos()
    {
        return startPos;
    }

    private void Update()
    {
        //count level distance
        if (counting)
        {
            distanceCount += Time.deltaTime;
        }
    }

    //return level distance
    public float GetDistance()
    {
        return distance;
    }

    public void SetCounting(bool value)
    {
        distanceCount = 0;
        counting = value;
    }
}
